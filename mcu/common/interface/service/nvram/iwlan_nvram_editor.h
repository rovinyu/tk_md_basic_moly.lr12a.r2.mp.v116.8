/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 *
 * Filename:
 * ---------
 *
 *
 * Project:
 * --------
 *   MAUI
 *
 * Description:
 * ------------
 *
 *
 * Author:
 * -------
 *
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 * removed!
 *
 * removed!
 * removed!
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/
#ifndef __IWLAN_NVRAM_EDITOR_H__
#define __IWLAN_NVRAM_EDITOR_H__

#ifndef NVRAM_NOT_PRESENT
#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

#include "iwlan_nvram_def.h"


#ifdef GEN_FOR_PC
BEGIN_NVRAM_DATA

/********************************************************************************************
* LID_NAME
*      NVRAM_EF_IWLAN_PROFILE_LID
* DESCRIPTION
*      All configuration related to IWLAN is defined here,
* INFOMATION
*      Can OTA Reset: Yes
*      Update Scenario: This LID will update once during task init or
*                       SIM plug out/plug in or setting parameters by engineer mode
*********************************************************************************************/
LID_BIT VER_LID(NVRAM_EF_IWLAN_PROFILE_LID)
    nvram_ef_iwlan_profile_record_struct *NVRAM_EF_IWLAN_PROFILE_TOTAL
    {
        wans_cfg: "Wireless Access Network Selection Configuration"{};
        wans_cfg.wans_ims_md_supporting_rat: "IMS support RAT. Bit[0]:lte Bit[1]:umts Bit[2]:gsm Bit[3]:1x Bit[4]:HRPD Bit[5]:EHRPD";
        wans_cfg.wans_ecc_ans_enable: "***";
        wans_cfg.wans_adm_ans_enable: "***";
        wans_cfg.wans_ims_vops_barring_enable: "When enable, only allow IMS on Cell with VOPS capability";
        wans_cfg.wans_ims_vops_setup_barring_enable: "When enable, only allow IMS setup on Cell with VOPS capability";
        wans_cfg.wans_ims_vops_idle_ho_barring_enable: "When enable, deny idle HO on Cell with non-VOPS capability";
        wans_cfg.wans_ims_23G_setup_barring_enable: "When enable, barring IMS setup on 2/3G";
        wans_cfg.wans_ims_roaming_barring_enable: "When enable, barring IMS on roaming Cell";
        wans_cfg.wans_xcap_roaming_barring_enable: "When enable, barring XCAP on roaming Cell";
        wans_cfg.wans_ims_wlan_roaming_barring_enable: "When enable, barring IMS on roaming WLAN";
        wans_cfg.wans_ims_sipcpi_barring_enable: "When enable, barring current RAT when SIP 403 error";
        wans_cfg.wans_mms_epdg_connected_barring_enable: "When enable, barring MMS on WLAN if there is no ePDG tunnel existed";
        wans_cfg.wans_mms_ims_pdn_connected_barring_enable: "When enable, barring MMS on WLAN if there is no IMS PDN existed";
        wans_cfg.wans_mms_wlan_only_barring_enable: "When enable, barring MMS on Cell if there is ePDG tunnel existed";
        wans_cfg.wans_mms_cell_only_barring_enable: "When enable, barring MMS on WLAN";
        wans_cfg.wans_xcap_epdg_connected_epdg_barring_enable: "When enable, barring XCAP on WLAN if there is no ePDG tunnel existed";
        wans_cfg.wans_xcap_wlan_only_barring_enable: "When enable, barring XCAP on Cell if there is ePDG tunnel existed";
        wans_cfg.wans_xcap_cell_only_barring_enable: "When enable, barring XCAP on WLAN";
        wans_cfg.wans_wlan_ike8192_barring_enable: "When enable, barring all APN on WLAN when received IKE8192 error";
        wans_cfg.wans_ims_cs_pref_enable: "When enable, prefer CS if CS quality is better than WLAN";
        wans_cfg.wans_ims_wlan_dereg_enable: "When enable, UE will de-register IMS on WLAN if prefer CS";
        wans_cfg.wans_wifi_call_roveout_alert_enable: "When enable, UE will display notification if UE is going to rove out WLAN during VoWiFi call";
        wans_cfg.wans_md_sig_report_duration: "Duration of monitor Cell signal quality for IWLAN";
        wans_cfg.wans_ims_pcscf_quality_enable: "When enable, P-CSCF connection quality monitoring will be turned on";
        wans_cfg.wans_ims_pcscf_state_bkoff_t: "Duration of handover back after handover out for pcscf connection quality poor";
        wans_cfg.wans_ims_evs_call_thr_offset: "Threshold offset when UE have on-going EVS call";
        wans_cfg.wans_1stpdn_lte_pref_apn_type: "For certain APN type, LTE preferred when PDN established 1st time after boot-up";
        wans_cfg.wans_ecc_ho_enable: "When enable, allow ECC handover";
        wans_cfg.wans_ims_dereg_dead_zone_enable: "When enable, UE will de-register IMS on WLAN, if WLAN RSSI value is below Low threshold and threre is no cellular coverage";
        wans_cfg.wans_bip_roaming_barring_enable: "When enable, barring BIP on roaming Cell";
        wans_cfg.wans_supl_roaming_barring_enable: "When enable, barring SUPL on roaming Cell";
        wans_cfg.wans_ims_dereg_dead_zone_incall_barring_enable: "When wans_ims_dereg_dead_zone_enable and this flag is enabled, UE will not de-register IMS on WLAN when call is active, if WLAN RSSI value is below Low threshold and threre is no cellular coverage";
        wans_cfg.wans_ims_dereg_dead_zone_mdpoor_barring_enable: "When wans_ims_dereg_dead_zone_enable and this flag is enabled, UE will de-register IMS on WLAN, if WLAN RSSI and MD signal value is below Low threshold";
        wans_cfg.wans_ims_roaming_ho_enable: "When disable, UE won't perform handover on roaming Cell";
        wans_cfg.wans_ims_roaming_incall_ho_enable: "When disable, UE won't perform handover during on-going IMS call on roaming Cell";
        wans_cfg.wans_ims_ecc_over_ims_ho_lte_to_wifi_enable: "When enable, while ECC call over IMS PDN, IMS PDN can hand over to WLAN";
        wans_cfg.wans_ims_ecc_over_ims_ho_wifi_to_lte_enable: "When enable, while ECC call over IMS PDN, IMS PDN can hand over to 3GPP";
        
        /***************************************/
        wans_cfg.throttling_cfg: "Handover Throttling Configuration"{};
        wans_cfg.throttling_cfg.wans_throttling_wlan_enable: "When enable, throttling handover back to WLAN for wans_wrovein_throttling_t";
        wans_cfg.throttling_cfg.wans_wrovein_throttling_t: "Duration of throttling handover back to WLAN";
        wans_cfg.throttling_cfg.wans_throttling_md_enable: "When enable, throttling handover back to Cell for wans_wroveout_throttling_t";
        wans_cfg.throttling_cfg.wans_wroveout_throttling_t: "Duration of throttling handover back to Cell";
        wans_cfg.throttling_cfg.wans_throttling_md_qual_override_enable: "When enabled, throttling of handover back to Cell is overridden when WLAN signal level or QoS goes bad";
        /***************************************/
        wans_cfg.meas_cfg: "Handover Measurement Delay Configuration"{};
        wans_cfg.meas_cfg.wans_wrovein_meas_t: "Duration of measurement delay before handover back to WLAN";
        wans_cfg.meas_cfg.wans_wroveout_meas_t: "Duration of measurement delay before handover back to Cell";
        /***************************************/
        wans_cfg.rat_hys_cfg: "RAT hysteresis during unstable signal temporal state" {};
        wans_cfg.rat_hys_cfg.wans_cell_unstable_hys_t: "Duration(ms) of cellular unstable signal hysteresis delay" {};
        wans_cfg.rat_hys_cfg.wans_wifi_unstable_hys_t: "Duration(ms) of wifi unstable signal hysteresis delay" {};
        /***************************************/
        wans_cfg.sig_cfg: "Signal Treshold Configurarion";
        /***************************************/
        wans_cfg.sig_cfg.wlan_sig_thr_cfg: "WLAN Signal Treshold Configurarion";
        /***************************************/
        wans_cfg.sig_cfg.wlan_sig_thr_cfg.wans_wlan_rssi_rove_poor_th: "Low threshold value of WLAN RSSI";
        wans_cfg.sig_cfg.wlan_sig_thr_cfg.wans_wlan_rssi_rove_fair_th: "High threshold value of WLAN RSSI";
        wans_cfg.sig_cfg.wlan_sig_thr_cfg.wans_wlan_rssi_ho_poor_th: "Low threshold value of WLAN RSSI when in call";
        wans_cfg.sig_cfg.wlan_sig_thr_cfg.wans_wlan_rssi_ho_fair_th: "High threshold value of WLAN RSSI when in call";
        wans_cfg.sig_cfg.wlan_sig_thr_cfg.wans_wlan_rssi_snr_poor_th: "Low threshold value of WLAN SNR";
        wans_cfg.sig_cfg.wlan_sig_thr_cfg.wans_wlan_rssi_snr_fair_th: "High threshold value of WLAN SNR";
        wans_cfg.sig_cfg.wlan_sig_thr_cfg.wans_wlan_rssi_no_cell_poor_th: "Low threshold value of WLAN RSSI when no cellular coverage";
        wans_cfg.sig_cfg.wlan_sig_thr_cfg.wans_wlan_rssi_no_cell_fair_th: "High threshold value of WLAN RSSI when no cellular coverage";
        /***************************************/
        wans_cfg.sig_cfg.md_sig_thr_cfg: "Cell Signal Treshold Configurarion";
        /***************************************/
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_lte_rsrp_poor_th: "Low threshold value of LTE RSRP";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_lte_rsrp_fair_th: "High threshold value of LTE RSRP";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_lte_rssnr_poor_th: "Low threshold value of LTE RSSNR";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_lte_rssnr_fair_th: "High threshold value of LTE RSSNR";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_lte_rsrq_poor_th: "Low threshold value of LTE RSRQ";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_lte_rsrq_fair_th: "High threshold value of LTE RSRQ";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_umts_rscp_poor_th: "Low threshold value of UMTS RSCP";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_umts_rscp_fair_th: "High threshold value of UMTS RSCP";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_umts_ecno_poor_th: "Low threshold value of UMTS ECNO";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_umts_ecno_fair_th: "High threshold value of UMTS ECNO";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_gsm_rssi_poor_th: "Low threshold value of GSM RSSI";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_gsm_rssi_fair_th: "High threshold value of GSM RSSI";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_1x_rssi_poor_th: "Low threshold value of 1x RSSI";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_1x_rssi_fair_th: "High threshold value of 1x RSSI";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_1x_ecio_poor_th: "Low threshold value of 1x ECIO";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_1x_ecio_fair_th: "High threshold value of 1x ECIO";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_evdo_rssi_poor_th: "Low threshold value of EVDO RSSI";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_evdo_rssi_fair_th: "High threshold value of EVDO RSSI";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_evdo_ecio_poor_th: "Low threshold value of EVDO ECIO";
        wans_cfg.sig_cfg.md_sig_thr_cfg.wans_evdo_ecio_fair_th: "High threshold value of EVDO ECIO";
        /***************************************/
        wans_cfg.qos_cfg: "QOS Configurarion"{};
        /***************************************/
        wans_cfg.qos_cfg.qos_rtp_cfg: "RTP QOS Configurarion"{};
        /***************************************/
        wans_cfg.qos_cfg.qos_rtp_cfg.wans_rtp_qos_enable: "When enable, RTP QOS monitoring will be turn on";
        wans_cfg.qos_cfg.qos_rtp_cfg.wans_ims_rtp_to_sterring_enable: "***";
        wans_cfg.qos_cfg.qos_rtp_cfg.wans_ims_wlan_voice_rtp_jitter_th: "Threshold value of RTP jitter loss when voice call in call";
        wans_cfg.qos_cfg.qos_rtp_cfg.wans_ims_wlan_video_rtp_jitter_th: "Threshold value of RTP jitter loss when video call in call";
        wans_cfg.qos_cfg.qos_rtp_cfg.wans_ims_rtp_wlan_pkt_loss_th: "Threshold value of RTP packet loss rate";
        wans_cfg.qos_cfg.qos_rtp_cfg.wans_ims_rtp_wlan_qos_bkoff_t: "Duration of handover back to WLAN after handover out";
        wans_cfg.qos_cfg.qos_rtp_cfg.wans_wlan_rtp_pkt_loss_th: "***";
        wans_cfg.qos_cfg.qos_rtp_cfg.wans_wlan_rtp_sampling_duration: "Duration of monitoring RTP status for IWLAN";
		wans_cfg.qos_cfg.qos_rtp_cfg.wans_wlan_rtcp_jitter_delay_th: "Threshold value of RTCP jitter delay";
        wans_cfg.qos_cfg.qos_rtp_cfg.wans_wlan_no_rtp_handover_enable: "When enable, No RTP timeout will trigger handover from WLAN to Cellular";
        /***************************************/
        wans_cfg.qos_cfg.qos_ping_cfg: "Ping QOS Configurarion"{};
        /***************************************/
        wans_cfg.qos_cfg.qos_ping_cfg.wans_ping_qos_enable: "When enable, Ping QOS monitoring will be turn on";
        wans_cfg.qos_cfg.qos_ping_cfg.wans_ping_interval: "Duration of monitoring Ping status for IWLAN";
        wans_cfg.qos_cfg.qos_ping_cfg.wans_ping_latency_th: "Threshold value of Ping latency";
        wans_cfg.qos_cfg.qos_ping_cfg.wans_ping_loss_th: "Threshold value of Ping packet loss rate";
        /***************************************/
        wans_cfg.qos_cfg.qos_dpd_cfg: "DPD QOS Configuration"{};
        /***************************************/
        wans_cfg.qos_cfg.qos_dpd_cfg.wans_dpd_qos_enable: "When enable, IWLAN can trigger DPD mechanism";
        wans_cfg.qos_cfg.qos_dpd_cfg.wans_ims_rtp_high_pkt_loss_for_dpd_th: "Threshold value of RTP high packet loss rate. If over  it, do DPD";
        wans_cfg.qos_cfg.qos_dpd_cfg.wans_ims_rtp_low_pkt_loss_for_dpd_th:  "Threshold value of RTP low packet loss rate. If under it, clear DPD";
        /***************************************/
        wans_cfg.qos_cfg.qos_reserved1: "***";
        /***************************************/
        wans_cfg.loc_cfg: "Location Configurarion"{};
        wans_cfg.loc_cfg.wans_location_enable: "When enable, loaction query will be turn on";
        wans_cfg.loc_cfg.wans_ims_wlan_no_location_barring_enable: "When enable, barring IMS on WLAN when no location information";
        wans_cfg.loc_cfg.wans_ims_wlan_block_in_ap_mode_when_home_enable: "When enable, barring IMS on WLAN when in home network";
        wans_cfg.loc_cfg.wans_ims_wlan_use_nv_md_srv_if_unknown_home_in_flight_mode: "When enable, IWLAN use NVRAM stored last MD srv state if unknown home network status in flight mode.";
        wans_cfg.loc_cfg.wans_ims_last_md_srv_state: "***";
        /***************************************/
        wans_cfg.wans_config_reserved1: "ocupied";
        wans_cfg.wans_config_pend_none_act_call_msg: "When enable, pend the none active call for switch RPL msg sequence in dual IMS mode";

    };

/********************************************************************************************
* LID_NAME
*      NVRAM_EF_WO_PROFILE_LID
* DESCRIPTION
*      All configuration related to WO & ePDG is defined here,
*      it allows user to configure to connect to ePDG server.
* INFOMATION
*      Can OTA Reset: Yes
*      Update Scenario: This LID will update once during task init or
*                       SIM plug out/plug in or setting parameters by engineer mode
*********************************************************************************************/
LID_BIT VER_LID(NVRAM_EF_WO_PROFILE_LID)
    nvram_ef_wo_profile_record_struct *NVRAM_EF_WO_PROFILE_TOTAL {
        epdg_fqdn: "Indicate the FQDN or IP of ePDG server (String format and ',' is delimiter)" {};
        ikev2if: "@INTERNAL_USE@ Indicate Wi-Fi interface name for IKEv2 used (String format)" {};
        select_info: "The PLMN list which is support ePDG service and the corresponding type to construct the ePDG FQDN (String format)" {};
        epdg_identifier: "The identifier list of Home-PLMN ePDG (FQDN List of Home-PLMN ePDG: String format and ',' is delimiter)" {};
        retry_vector: "The string vector including error code range and retry timer value used for data retry (String format)" {};
        ike_algo: "Specifie the IKE algorithms types that are used by the UE (String format)" {};
        esp_algo: "Specifie the ESP algorithms types that are used by the UE (String format)" {};
        ike_rekey_timer: "Lifetime (in seconds) of an IKE SA. UE should do IKEv2 rekey procedure before the lifetime of the IKE SA" {};
        esp_rekey_timer: "Lifetime (in seconds) of an ESP SA. UE should do ESP rekey procedure before the lifetime of the ESP SA" {};
        rekey_margin: "Time (in seconds) before the rekeying should start. Before the key of IKE/ESP SA was expired, IKEv2/ESP should start the rekey job. The rekey timing should be between (lifetime - margin) to (lifetime)." {};
        dpd_timer: "Time period in seconds after which the UE shall perform the DPD" {};
        keep_timer: "Time in seconds after which the UE shall send NATT keep-alive messages" {};
        esp_setup_time: "Maximum time in seconds for 1 tunnel setup; set to 0 will be auto-generated by retransmission timeout" {};
        pdn_setup_time: "@INTERNAL_USE@ Maximum time in seconds for 1 initial attach or handover request" {};
        cert_used: "" {
            cert_used: 8 "Used to determine if certificates are used to authenticate the ePDG server during tunnel establishment procedures" {
                0: "Disable";
                1: "Enable";
            };
        };
        urlcert: "" {
            urlcert: 8 "Support HTTP cert or not (Hash and URL of X.509)" {
                0: "Disable";
                1: "Enable";
            };
        };
        ocsp: "" {
            ocsp: 8 "Support OCSP in certificate" {
                0: "Disable";
                1: "Enable";
            };
        };
        nocert: "" {
            nocert: 8 "UE did not expect certificate from ePDG" {
                0: "Disable";
                1: "Enable";
            };
        };
        skip_check_cert: "" {
            skip_check_cert: 8 "Skip to check Certificate from ePDG even UE recevid certificate. When IODT/IOT, certificate may be not correct from ePDG. This option provide tester skip the certificate issue in test stage." {
                0: "Disable";
                1: "Enable";
            };
        };
        noid: "" {
            noid: 8 "Skip to check IDr in Certificate from ePDG" {
                0: "Disable";
                1: "Enable";
            };
        };
        force_tsi_64: "" {
            force_tsi_64: 8 "Replace the value in TSi with the prefix_64bits internal IPv6 address  for IPv6 only, if tsi_full and tsi_64 all enabled, apply tsi_full first and then tsi_64" {
                0: "Disable";
                1: "Enable";
            };
        };
        force_tsi_full: "" {
            force_tsi_full: 8 "Replace the IP address in TSi with the whole IP address (full IPv6 address and IPv4 addresss)" {
                0: "Disable";
                1: "Enable";
            };
        };
        use_cfg_vip: "" {
            use_cfg_vip: 8 "Install the address which in requested INTERNAL_IP*_ADDRESS configuration payload (not from responded one)" {
                0: "Disable";
                1: "Enable";
            };
        };
        reauth_addr: "" {
            reauth_addr: 8 "Do IKEv2 re-authentication when address changed" {
                0: "Disable";
                1: "Enable";
            };
        };
        dpd_no_reply: "" {
            dpd_no_reply: 8 "Do not reply DPD requests from ePDG" {
                0: "Disable";
                1: "Enable";
            };
        };
        pre_post_ping: "@INTERNAL_USE@" {
            pre_post_ping: 8 "ping ePDG before establishing PDN (This config is no use anymore)" {
                0: "Disable";
                1: "Enable";
            };
        };
        log_level: "" {
            log_level: 8 "Debugging log level" {
                0: "Level 0";
                1: "Level 1";
                2: "Level 2";
                3: "Level 3";
                4: "Level 4";
            };
        };
        wdrv_keep_alive: "" {
            wdrv_keep_alive: 8 "Sent NAT Keep-alive packets by Wi-Fi driver, and AP can go to sleep (Depend on if Wi-Fi driver supports this feature or not)" {
                0: "Disable";
                1: "Enable";
            };
        };
        fragment: "" {
            fragment: 8 "Support IKEv2 Fragmentation" {
                0: "Disable";
                1: "Enable";
            };
        };
        mobike: "" {
            mobike: 8 "Support Mobike" {
                0: "Disable";
                1: "Enable";
            };
        };
        oos: "Maximum time in seconds for WiFi out-of-service (OOS) timer to keep PDNs existed" {};
        retrans_to: "Retransmission timeout in seconds for IKEv2 packet" {};
        retrans_tries: "Maximum number of times a UE shall retransmit an IKEv2 packet if it does not receive a response" {};
        retrans_base: "Base to use for IKEv2 calculating exponential back off" {};
        mtu: "MTU size (byte) of the ESP tunnel" {};
        mss4_off: "TCP Maximum Segment Size offset (byte) of IPv4 for the ESP tunnel" {};
        mss6_off: "TCP Maximum Segment Size offset (byte) of IPv6 for the ESP tunnel" {};
        cust_pcscf_4: "Configuration Payload Attribute Type value for P_CSCF_IP4_ADDRESS" {};
        cust_pcscf_6: "Configuration Payload Attribute Type value for P_CSCF_IP6_ADDRESS" {};
        cust_imei_cp: "Configuration Payload Attribute Type value for IMEI" {};
        port: "@INTERNAL_USE@ UDP port number of IKEv2" {};
        port_natt: "@INTERNAL_USE@ UDP port number of IKEv2 NAT-T" {};
        cpa_nm: "Number of INTERNAL_IP4_NETMASK configuration payloads will be added in IKE_AUTH" {};
        cpa_dns4: "Number of INTERNAL_IP4_DNS configuration payloads will be added in IKE_AUTH" {};
        cpa_dns6: "Number of INTERNAL_IP6_DNS configuration payloads will be added in IKE_AUTH" {};
        no_ic: "" {
            no_ic: 8 "Don't add INIT_CONTACT notify payload in IKEv2 request" {
                0: "Disable";
                1: "Enable";
            };
        };
        no_eap: "" {
            no_eap: 8 "Don't add EAPONLY notify payload in IKEv2 request" {
                0: "Disable";
                1: "Enable";
            };
        };
        ike_dscp: "DSCP(Differentiated Services Code Point) value for outgoing IKEv2 packets sent from this connection. The value is a 6-bits decimal (0~63)." {};
        IDi: "" {
            IDi: 8 "Specifies the format of the IDi that should be used in the IKEv2 authentication message(s)" {
                0: "ID_RFC822_ADDR is the 3GPP standard IDi format.";
                1: "ID_RFC822_ADDR_MAC is the IDi and EAP Auth ID both using ID_RFC822_ADDR-based format with inclusion of the WiFi MAC";
                2: "ID_RFC822_ADDR_MAC_EAP_NO_MAC is the IDi using ID_RFC822_ADDR_MAC-based format, but EAP Auth ID using ID_RFC822_ADDR-based format.";
            };
        };
        IDr: "" {
            IDr: 8 "Specifies the format of the IDr that should be used in the IKEv2 authentication message(s)" {
                0: "ID_FQDN";
                1: "ID_KEY_ID";
            };
        };
        leftauth: "" {
            leftauth: 8 "The IKEv2 authentication method" {
                0: "AUTH_EAP";
                1: "AUTH_PSK";
                2: "AUTH_PUBKEY";
            };
        };
        eap: "" {
            eap: 8 "The EAP algorithm in Authentication" {
                0: "EAP-AKA";
                1: "EAP-SIM";
                2: "EAP-AKA'";
            };
        };
        fastreauth: "@INTERNAL_USE@" {
            fastreauth: 8 "Support fast re-authentication" {
                0: "Disable";
                1: "Enable";
            };
        };
        dns_timer: "Maximum time in seconds. DNS query will not be performed before timeout. The timer only starts when UE fails to connect to ePDG." {};
        dns_type: "" {
            dns_type: 8 "Choose the preference of address type got from DNS" {
                0: "DNS_ADDR_V4";
                1: "DNS_ADDR_V6";
                2: "DNS_ADDR_V4V6";
                3: "DNS_ADDR_V6V4";
            };
        };
        dns_max_count: "Maximum number for DNS results" {};
        dns_cache: "" {
            dns_cache: 8 "Cache the DNS result for last connected ePDG server" {
                0: "Disable";
                1: "Enable";
            };
        };
        redirect: "@INTERNAL_USE@" {
            redirect: 8 "Support IKEv2 redirect feature" {
                0: "Disable";
                1: "Enable";
            };
        };
        max_redirects: "@INTERNAL_USE@ Maximum redirect times for IKEv2 redirect" {};
        redirect_loop_detect: "@INTERNAL_USE@ Time in seconds to detect the IKEv2 redirect loop" {};
        detach_soft_timer: "Time in seconds after which the UE shall reply detach resposne" {};
        detach_hard_timer: "Time in seconds after which the UE shall stop detaching and local cleanup" {};
        fdpd_retrans_to: "@INTERNAL_USE@ Retransmission timeout in seconds for force DPD" {};
        fdpd_retrans_tries: "@INTERNAL_USE@ Maximum number of times a UE shall retransmit a force DPD packet if it does not receive a response" {};
        fdpd_retrans_base: "@INTERNAL_USE@ Base to use for force DPD calculating exponential back off" {};
        certreq_critical: "" {
            certreq_critical: 8 "Mark CERTREQ_CRITICAL bit in IKEv2 request" {
                0: "Disable";
                1: "Enable";
            };
        };
        pcscf_restore: "" {
            pcscf_restore: 8 "Add PCSCF_RESTORE notify payload in IKEv2 request" {
                0: "Disable";
                1: "Enable";
            };
        };
        liveness_check: "" {
            liveness_check: 8 "Add LIVENESS_CHECK configuration payload in IKEv2 request" {
                0: "Disable";
                1: "Enable";
            };
        };
        device_identity: "" {
            device_identity: 8 "Add DEVICE_IDENTITY notify payload in IKEv2 request" {
                0: "Disable";
                1: "Enable";
            };
        };
        abort_mode: "" {
            abort_mode: 8 "Abort mode of IKEv2 setup: Disable - delete IKEv2 SA when setup on-going; Enable - delete IKEv2 SA after setup completed" {
                0: "Disable";
                1: "Enable";
            };
        };
        ho_ip_disc: "" {
            ho_ip_disc: 8 "Allow LTE to Wi-Fi Handover IP discontinuity" {
                0: "Disable";
                1: "Enable";
            };
        };
        emerg_epdg: "" {
            emerg_epdg: 8 "Support Emergency EPDG selection" {
                0: "EMERG_EPDG_SELECT_NONE";
                1: "EMERG_EPDG_SELECT";
            };
        };
        try_epdg_policy: "@INTERNAL_USE@" {
            try_epdg_policy: 8 "Choose the policy of trying ePDG's IP on each PDN setup" {
                0: "TRY_ALL_EPDG_IP";
                1: "TRY_ONE_EPDG_IP";
            };
        };
        imei_format: "" {
            imei_format: 8 "Choose preferred format for IMEI in IKEv2 Configuration Payload" {
                0: "IMEI_BCD";
                1: "IMEI_STRING";
            };
        };
        leave_standby_dpd: "" {
            leave_standby_dpd: 8 "Perform DPD after leaving standby mode" {
                0: "Disable";
                1: "Enable";
            };
        };
        send_del_ike_auth_ntfy_err: "" {
            send_del_ike_auth_ntfy_err: 8 "Send delete IKEv2 SA after specific notify error" {
                0: "Disable";
                1: "Enable";
            };
        };
    };

/********************************************************************************************
* LID_NAME
*      NVRAM_EF_IWLAN_PROVISION_PROFILE_LID
* DESCRIPTION
*      All configuration related to IWLAN provision is defined here,
* INFOMATION
*      Can OTA Reset: Yes
*      Update Scenario: This LID will update once during task init or
*                       provision by operator
*********************************************************************************************/
LID_BIT VER_LID(NVRAM_EF_IWLAN_PROVISION_PROFILE_LID)
    nvram_ef_iwlan_provision_profile_record_struct *NVRAM_EF_IWLAN_PROVISION_PROFILE_TOTAL
    {

    };

/********************************************************************************************
* LID_NAME
*      NVRAM_EF_DRP_IWLAN_PROFILE_LID
* DESCRIPTION
*      Mirror to NVRAM_EF_IWLAN_PROFILE_LID,
* INFOMATION
*      Can OTA Reset: Yes
*      Update Scenario: This is for IWLAN runtime context dump
*
*********************************************************************************************/
LID_BIT VER_LID(NVRAM_EF_DRP_IWLAN_PROFILE_LID)
    nvram_ef_iwlan_profile_record_struct *NVRAM_EF_DRP_IWLAN_PROFILE_TOTAL
    {
    };

/********************************************************************************************
* LID_NAME
*      NVRAM_EF_DRP_WO_PROFILE_LID
* DESCRIPTION
*      Mirror to NVRAM_EF_WO_PROFILE_LID,
* INFOMATION
*      Can OTA Reset: Yes
*      Update Scenario: This is for WO runtime context dump
*
*********************************************************************************************/
LID_BIT VER_LID(NVRAM_EF_DRP_WO_PROFILE_LID)
    nvram_ef_wo_profile_record_struct *NVRAM_EF_DRP_WO_PROFILE_TOTAL
    {
    };




END_NVRAM_DATA
#endif	/*GEN FOR PC*/

#ifdef __cplusplus
}
#endif

#endif  /* !NVRAM_NOT_PRESENT */
#endif  /* __IWLAN_NVRAM_EDITOR_H__ */
